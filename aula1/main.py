import util as ut
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 

def mean(A):
	media = 0.0
	for i in range(0,len(A)):
		for j in range(0,len(A[0])):
			media += A[i][j]

	media = media/len(A[0])
	return media

def Bias_Fix_Linear(A):
	B = ut.copy_matrix(A)
	for i in range(0,len(B)+1):
		if i == len(B):
			B.append([])
			for j in range(0,len(B[0])):
				B[i].append(1)
	return B

def Bias_Fix_Quadratic(A):
	B = []
	for i in range(0,3):
		B.append([])

	for i in range(0,3):
		if i == 0:
			for j in range(0,len(A[0])):
				B[i].append(A[i][j]**2)
		elif i == 1:
			for j in range(0,len(A[0])):
				B[i].append(A[0][j])
		else:
			for j in range(0,len(A[0])):
				B[i].append(1)
	return B

def Bias_Fix_Mult(A,C):
	B = []
	for i in range(0,3):
		B.append([])

	for i in range(0,3):
		if i == 0:
			for j in range(0,len(A[0])):
				B[i].append(C[j])
		elif i == 1:
			for j in range(0,len(A[0])):
				B[i].append(A[0][j])
		else:
			for j in range(0,len(A[0])):
				B[i].append(1)
	return B

def calc(V,M):
	result = None
	vec = [V]
	result = ut.matmult(vec,M)[0][0]
	return result

def vec2matx(V):
	Mat = []
	Mat.append(V)
	return Mat

def Weight_create(Y):
	W = []
	ych = mean(Y)
	for i in range(0,len(Y[0])):
		w_ = abs(1.0/(Y[0][i]-ych))
		W.append(w_)

	return W

def weight_Mult(W,X):
	A = ut.copy_matrix(X)
	for i in range(0,len(W[0])):
		A[0][i] *= W[0][i]
	
	return A

def Normalize(X):
	A = ut.copy_matrix(X)
	media = mean(X)
	for i in range(len(A[0])):
		A[0][i] -= media

	return A


def LSM_Linear(X,Y):
	Mx = vec2matx(X)
	Mx = (Bias_Fix_Linear(Mx))
	My = vec2matx(Y)
	My = Normalize(My)

	p1 = ut.matmult(Mx,ut.transposta(Mx))
	p1 = ut.Inversa(p1)
	p2 = ut.matmult(Mx,ut.transposta(My))

	B = ut.matmult(p1,p2)

	assist = ut.transposta(Mx)
	result = []
	for i in range(0,len(Mx[0])):
		result.append(calc(assist[i],B))

	return result

def LSM_Quadratic(X,Y):
	Mx = vec2matx(X)
	Mx = (Bias_Fix_Quadratic(Mx))
	My = vec2matx(Y)

	p1 = ut.matmult(Mx,ut.transposta(Mx))
	p1 = ut.Inversa(p1)
	p2 = ut.matmult(Mx,ut.transposta(My))

	B = ut.matmult(p1,p2)

	assist = ut.transposta(Mx)
	result = []
	for i in range(0,len(Mx[0])):
		result.append(calc(assist[i],B))

	return result

def LSM_Multi(X,Y,Z):
	Mx = vec2matx(X)
	Mx = (Bias_Fix_Mult(Mx,Z))
	My = vec2matx(Y)

	p1 = ut.matmult(Mx,ut.transposta(Mx))
	p1 = ut.Inversa(p1)
	p2 = ut.matmult(Mx,ut.transposta(My))

	B = ut.matmult(p1,p2)

	assist = ut.transposta(Mx)
	result = []
	for i in range(0,len(Mx[0])):
		result.append(calc(assist[i],B))

	return result

def LSM_Robust(X,Y):
	Mx = vec2matx(X)
	Mx = Normalize(Mx)
	Mx = (Bias_Fix_Linear(Mx))
	My = vec2matx(Y)

	#W = Weight_create(My)
	W = []
	for i in range(0,len(My[0])):
		W.append(1)

	Mw = vec2matx(W)

	xw = weight_Mult(Mw,Mx)
	yw = weight_Mult(Mw,My)

	p1 = ut.matmult(xw,ut.transposta(Mx))
	p1 = ut.Inversa(p1)
	p2 = ut.matmult(Mx,ut.transposta(yw))

	B = ut.matmult(p1,p2)

	assist = ut.transposta(Mx)
	result = []
	for i in range(0,len(Mx[0])):
		result.append(calc(assist[i],B))

	return result

def create_plot(X,Y,Result):
	plt.plot(X,Y,'ro')
	plt.plot(X,Result)
	plt.show()

def plot_3d(X,Y,Z,Result):
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.scatter(X, Z, Y, marker='o')
	x = np.array(X)
	y = np.array(Y)
	res = np.array(Result)
	ax.plot(x, y, res)
	plt.show()

if __name__ == '__main__':
	file = open("books.csv","r")

	Vx = []
	Vy = []
	Vz = []
	for i in file:
		i = i.replace(",",".")
		x, z, y = i.split(';')
		Vx.append(int(x))
		Vy.append(int(y))
		Vz.append(int(z))

	Result = LSM_Multi(Vx,Vy,Vz)

	plot_3d(Vx,Vz,Vy,Result)










	