'''
Autor: Thiago Spilborghs Bueno Meyer
Descrição: Métodos com matrizes para realizar o Least Square Method.
    - Print_matrix -> Imprime a matriz para uma melhor visualização
    - matmult -> Realiza a multiplicação de matrizes
    - transposta -> Retorna a matriz transposta
    - copy_matrix -> copia a matriz dada para uma nova
    - Cof -> cria a matriz de cofatores
    - gen_sub -> cria uma sub matriz, baseada em uma matriz anterior. utilizada no cálculo do Cof
    - det -> calcula o determinante de uma matriz
    - Inversa -> calcula a matriz inversa de uma matriz A
'''
def Print_matrix(A):
    for i in range(0,len(A)):
        print '| ' ,
        for j in range(0,len(A[0])):
            print A[i][j] ,
            if j == len(A[0])-1:
                print " |"
            else:
                print " , " ,

def matmult(m1,m2):
    r=[]
    m=[]
    for i in range(len(m1)):
        for j in range(len(m2[0])):
            sums=0
            for k in range(len(m2)):
                sums=sums+(m1[i][k]*m2[k][j])
            r.append(sums)
        m.append(r)
        r=[]
    return m

def transposta(A):
    result = [[A[j][i] for j in range(len(A))] for i in range(len(A[0]))]
    return result

def copy_matrix(A):
    B = []
    for i in range(0,len(A)):
        B.append([])
        for j in range(len(A[0])):
            B[i].append(A[i][j])
    return B

def Cof(A):
    if len(A) != len(A[0]):
        print "Sorry, but you should use a AxA matrix, not a AxB matrix"
        return None
    
    elif len(A) == 2 and len(A[0]) == 2:
        B = copy_matrix(A) #Matriz para uso e alteracao
        B[0][0] = ((-1.)**(0+0)) * A[1][1]
        B[0][1] = ((-1.)**(0+1)) * A[1][0]
        B[1][0] = ((-1.)**(1+0)) * A[0][1]
        B[1][1] = ((-1.)**(1+1)) * A[0][0]
        return B
    
    else:
        B = copy_matrix(A)
        x = len(A)
        y = len(A[0])
        for i in range(0,x):
            for j in range(0,y):
                su = gen_sub(A,i,j)
                det_IJ = det(su)
                B[i][j] = ((-1.)**(i+j)) * det_IJ

        return B

def gen_sub(A,linha,coluna):
    sub = []
    contlin = 0
    for i in range(0,len(A)):
        if i != linha:
            sub.append([])
            for j in range(0,len(A[0])):
                if j != coluna:
                    sub[contlin].append(A[i][j])
            contlin += 1

    return sub

def det(A, total=0):
    indices = list(range(len(A)))
    if len(A) != len(A[0]):
        print "Sorry, but you should use a AxA matrix, not a AxB matrix"
        return None
    
    if len(A) == 2 and len(A[0]) == 2:
        val = A[0][0] * A[1][1] - A[1][0] * A[0][1]
        return val

    for fc in indices:
        As = copy_matrix(A)
        As = As[1:] 
        height = len(As)
 
        for i in range(height): 

            As[i] = As[i][0:fc] + As[i][fc+1:] 
 
        sign = (-1) ** (fc % 2)

        sub_det = det(As)

        total += sign * A[0][fc] * sub_det 
 
    return total

def Inversa(A):
    determ = det(A)
    A_inv = transposta(Cof(A))
    for i in range(0,len(A)):
        for j in range(0,len(A[0])):
            A_inv[i][j] *= 1./determ

    return A_inv

