import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def calculo(A,B,Results):
    res = np.zeros(len(A))
    for i in range(0,len(A)):
        v = A[i]*Results[0] + B[i] * Results[1] + Results[2]
        res[i]+=v
    return res
# Model Evaluation - RMSE
def rmse(Y, Y_pred):
    rmse = np.sqrt(sum((Y - Y_pred) ** 2) / len(Y))
    return rmse

# Model Evaluation - R2 Score
def r2_score(Y, Y_pred):
    mean_y = np.mean(Y)
    ss_tot = sum((Y - mean_y) ** 2)
    ss_res = sum((Y - Y_pred) ** 2)
    r2 = 1 - (ss_res / ss_tot)
    return r2

def gradient_descent(X, Y, B, alpha, iterations):
    cost_history = [0] * iterations
    m = len(Y)
    
    for iteration in range(iterations):
        # Hypothesis Values
        h = X.dot(B)
        # Difference b/w Hypothesis and Actual Y
        loss = h - Y
        # Gradient Calculation
        gradient = X.T.dot(loss) / m
        # Changing Values of B using Gradient
        B = B - alpha * gradient
        # New Cost Value
        cost = cost_function(X, Y, B)
        cost_history[iteration] = cost
        
    return B, cost_history

def cost_function(X, Y, B):
    m = len(Y)
    J = np.sum((X.dot(B) - Y) ** 2)/(2 * m)
    return J

Books = []
Classes = []
Grade = []
file = open("books.csv",'r')
for i in file:
    b, c, g = i.split(';')
    Books.append(int(b))
    Classes.append(int(c))
    Grade.append(int(g))


# Ploting the scores as scatter plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(Books, Classes, Grade, color='#ef1234')

#plt.show()

m = len(Books)
x0 = np.ones(m)
X = np.array([x0, Books, Classes]).T
# Initial Coefficients
B = np.array([0, 0, 0])
Y = np.array(Grade)
alpha = 0.0001


inital_cost = cost_function(X, Y, B)
print("Initial Cost")
print(inital_cost)


# 100000 Iterations
newB, cost_history = gradient_descent(X, Y, B, alpha, 10000)

# New Values of B
print("New Coefficients")
print(newB)

surf = calculo(Books,Classes,newB)
ax.plot(Books,Classes,surf)

plt.show()
# Final Cost of new B
print("Final Cost")
print(cost_history[-1])


Y_pred = X.dot(newB)

print("RMSE")
print(rmse(Y, Y_pred))
print("R2 Score")
print(r2_score(Y, Y_pred))