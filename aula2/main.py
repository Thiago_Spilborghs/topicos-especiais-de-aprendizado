# -*- coding: utf-8 -*-
import numpy as np
import util as ut
import cubic as cb
import matplotlib.pyplot as plt
import math
from mpl_toolkits.mplot3d import Axes3D
'''
Autor: Thiago S B Meyer
Descrição: PCA
Data: 31/10/2019
'''
def mean(V):
	media = 0.
	for i in V:
		media +=i
	media = media/len(V)
	return media

def Data_Adjust(X):
	x_ = mean(X)
	A = []
	for i in X:
		A.append(i-x_)
	return A

def W2Choose(X):
	Maior = X[0]
	index = 0
	for i in range(1,len(X)):
		if X[i] > Maior:
			Maior = X[i]
			index = i

	return index

def Remove_Value(X,value):
	A = []
	for i in X:
		if i != value:
			A.append(i)
	return A

def Solver(X,Y):
	XM = ut.copy_matrix(X)
	n = len(X)
	YM = ut.copy_matrix(Y)
 
	indices = list(range(n)) 
	for fd in range(n): 
		fdScaler = 1.0 / XM[fd][fd]
		
		for j in range(n): 
			XM[fd][j] *= fdScaler
		YM[fd][0] *= fdScaler
		 
		
		for i in indices[0:fd] + indices[fd+1:]: 
			crScaler = XM[i][fd] 
			for j in range(n): 
				XM[i][j] = XM[i][j] - crScaler * XM[fd][j]
			YM[i][0] = YM[i][0] - crScaler * YM[fd][0]

	return ut.matmult(X,YM)

def Create_FeatureVec(X,index):
	return X[index]

def Eigenvalues2x2(M):
	a = M[0][0]
	b = M[0][1]
	c = M[1][0]
	d = M[1][1]
	# Formula -> Lambda^2 +(-a-d)*Lambda - (c*b-a*d) = 0
	Delta = (-a-d)**2. - 4.*1*(c*b - a*d)
	Lambda1 = (-(-a-d) + Delta**0.5)/2*1.0
	Lambda2 = (-(-a-d) - Delta**0.5)/2*1.0
	V = [Lambda1,Lambda2]
	return V

def Eigenvectors2x2(M,Lbda):
	Egvct = []
	a = M[0][0]
	b = M[0][1]
	c = M[1][0]
	d = M[1][1]
	A = [[a,b],[c,d]]
	B = [[0],[0]]
	#Result = Solver(A,B)
	#Assumiremos x como 1 para calculo do Y
	x = 1
	# Formula geral Y = ((Lbda-a)/b) * x
	Y = (((a-Lbda)-b)*x)/((d-Lbda)-c)
	# Formula geral X = ((Lbda-d)/c)* Y
	# Será utilizado Y calculado
	X = (b*Y)/(Lbda-a)
	S = math.sqrt(X**2+Y**2)
	print S
	X = X/S
	Y = Y/S
	Egvct.append(X)
	Egvct.append(Y)
	return Egvct

def Eigenvalues3x3(M):
	a = M[0][0]
	b = M[0][1]
	c = M[0][2]
	d = M[1][0]
	e = M[1][1]
	f = M[1][2]
	g = M[2][0]
	h = M[2][1]
	i = M[2][2]

	A = -1.
	B = i + a - e
	C = -a*i - e*i - a*e -c*g - f*h - b*d
	D = a*e*i + b*f*g + c*d*f - c*g*e - f*h*a -b*d*i

	if D == 0:
		Delta = (B)**2. - 4.*A*C
		Lambda1 = (-(B) + Delta**0.5)/2.0*A
		Lambda2 = (-(B) - Delta**0.5)/2.0*A
		Lambda3 = 0
		Lbda = [Lambda1,Lambda2,Lambda3]
	else:
		Lbda = cb.solve(A,B,C,D)
		return Lbda


def Eigenvectors3x3(M,Lbda):
	Egvct = []
	a = M[0][0]
	b = M[0][1]
	c = M[0][2]
	d = M[1][0]
	e = M[1][1]
	f = M[1][2]
	g = M[2][0]
	h = M[2][1]
	i = M[2][2]
	
	
	x = 1
	y = 2
	z = (g*x+h*y)/(Lbda-i)
	y = ((d*x)+(f*z))/(Lbda-e)
	x = (b*y+c*z)/(Lbda-a)
	S = math.sqrt(x**2+y**2+z**2)
	X = x/S
	Y = y/S
	Z = z/S
	Egvct.append(X)
	Egvct.append(Y)
	Egvct.append(Z)
	return Egvct

def Cov(X,Y):
	x_ = mean(X)
	y_ = mean(Y)
	Cov = 0
	for i in range(0,len(X)):
		Cov += (X[i]-x_) * (Y[i]-y_)
	Cov = Cov/(len(X)-1)
	return Cov

def Cov_Mat(V):
	CovM = []
	for i in range(0,len(V)):
		CovM.append([])
		for j in range(0,len(V)):
			c = Cov(V[i],V[j])
			CovM[i].append(c)
	return CovM 

def PCA(X,Y,Z,size):
	V = []
	VX = Data_Adjust(X)
	V.append(VX)
	VY = Data_Adjust(Y)
	V.append(VY)
	if len(Z) != 0:
		VZ = Data_Adjust(Z)
		V.append(VZ)

	Matrix = Cov_Mat(V)
	if size ==2:
		eigval = Eigenvalues2x2(Matrix)
		eigvec = []
		for i in eigval:
			calc = Eigenvectors2x2(Matrix,i)
			eigvec.append(calc)
		index = W2Choose(eigval)
		return [eigvec,index]
	else:
		eigval = Eigenvalues3x3(Matrix)
		eigvec = []
		for i in eigval:
			calc = Eigenvectors3x3(Matrix,i)
			eigvec.append(calc)
		index = []
		index.append(W2Choose(eigval))
		eigval = Remove_Value(eigval,eigval[index[0]])
		index.append(W2Choose(eigval))
		return [eigvec,index]

if __name__ == '__main__':
	file = open("books.csv")
	Vx = []
	Vy = []
	Vz = []
	size = 2
	for i in file:
		i = i.replace(',','.')
		i = i.split(';')
		Vx.append(float(i[0]))
		Vy.append(float(i[1]))
		if len(i) == 3:
			Vz.append(float(i[2]))
			size = 3

	Eigvecs, index = PCA(Vx,Vy,Vz,size)
	if size == 2:
		Xplot = Data_Adjust(Vx)
		Yplot = Data_Adjust(Vy)
		FinalDt = [Xplot,Yplot]
		FinalDt = ut.transposta(FinalDt)
		Data = ut.matmult(FinalDt,Eigvecs)
		ut.Print_matrix(Data)

		Eg1 = []
		for i in range(-10,10):
			Eg1.append([Eigvecs[index][0]*i,Eigvecs[index][1]*i])

		Eg1 = ut.transposta(Eg1)

		Eg2 = []
		for i in range(-20,20):
			Eg2.append([Eigvecs[(index+1)%2][0]*i,Eigvecs[(index+1)%2][1]*i])
		Eg2 = ut.transposta(Eg2)

		plt.plot(Xplot,Yplot,'ro')
		plt.plot(Eg2[0],Eg2[1],linestyle='--',color='gray')
		plt.plot(Eg1[0],Eg1[1],linestyle='-',color='green')
		plt.show()
	else:
		Xplot = Data_Adjust(Vx)
		Yplot = Data_Adjust(Vy)
		Zplot = Data_Adjust(Vz)
		FinalDt = [Xplot,Yplot,Zplot]
		FinalDt = ut.transposta(FinalDt)
		Data = ut.matmult(FinalDt,Eigvecs)
		
		Eig1 = []
		for i in range(-50,200):
			a = Eigvecs[index[0]][0] * i
			b = Eigvecs[index[0]][1] * i
			c = Eigvecs[index[0]][2] * i
			Vc = [a,b,c]
			Eig1.append(Vc)

		Eig2 = []
		for i in range(-10,10):
			a = Eigvecs[index[1]][0] * i
			b = Eigvecs[index[1]][1] * i
			c = Eigvecs[index[1]][2] * i
			Vc = [a,b,c]
			Eig2.append(Vc)

		helper = [0,1,2]
		helper = Remove_Value(helper,index[0])
		helper = Remove_Value(helper,index[1])
		Eig3 = []
		for i in range(-25,50):
			a = Eigvecs[helper[0]][0] * i
			b = Eigvecs[helper[0]][1] * i
			c = Eigvecs[helper[0]][2] * i
			Vc = [a,b,c]
			Eig3.append(Vc)

		fig = plt.figure()
		ax = Axes3D(fig)
		PlotEig = ut.transposta(Eig1)
		PlotEig2 = ut.transposta(Eig2)
		PlotEig3 = ut.transposta(Eig3)
		ax.scatter(Xplot,Yplot,Zplot,color='#ef1234')
		ax.plot(PlotEig[0],PlotEig[1],PlotEig[2],'blue')
		ax.plot(PlotEig2[0],PlotEig2[1],PlotEig2[2],'green')
		ax.plot(PlotEig3[0],PlotEig3[1],PlotEig3[2],'gray')
		plt.show()

