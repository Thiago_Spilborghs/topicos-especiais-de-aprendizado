import util as ut
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def Mat2Vec(M):
	V = []
	for i in M:
		for j in i:
			V.append(j)
	return V

def f1(X,weight):
	v = ut.matmult([X], ut.transposta([weight[1:]]))[0][0] + weight[0]
	resp = None
	if v > 0:
		resp = 1
	else:
		resp = 0

	return resp 

class Perceptron():
	
	def __init__(self,func,n_inputs,epochs=100,L_rate = 0.01):
		self.epoch = epochs
		self.L_rate = L_rate
		self.function = func
		self.w = []
		for i in range(0,n_inputs+1):
			self.w.append(0)

	def train(self,t_set,label):
		for i in range(self.epoch):
			for inputs, l in zip(t_set, label):
				pred = self.predict(inputs)
				self.w[1:] += self.L_rate * (l - pred) * inputs
				self.w[0] += self.L_rate * (l - pred)

	def predict(self,Input):
		resp = self.function(Input,self.w)
		return resp

if __name__ == '__main__':
	#And Logic Perceptron
	training_inputs = []
	training_inputs.append(np.array([1, 1]))
	training_inputs.append(np.array([1, 0]))
	training_inputs.append(np.array([0, 1]))
	training_inputs.append(np.array([0, 0]))

	labels = np.array([1, 0, 0, 0])

	P = Perceptron(func=f1,n_inputs=2)
	P.train(training_inputs,labels)

	V = ['rs','b^']
	for teste in training_inputs:
		pred = P.predict(teste)
		plt.plot(teste[0],teste[1],V[pred])
	plt.xlabel('X')
	plt.ylabel('Y')
	plt.title('AND Logic Perceptron')
	red = mpatches.Patch(color='red', label='False = 0')
	blue = mpatches.Patch(color='blue', label='True = 1')
	plt.legend(handles=[red,blue])
	plt.show()

	#Or Logic Perceptron
	training_inputs = []
	training_inputs.append(np.array([1, 1]))
	training_inputs.append(np.array([1, 0]))
	training_inputs.append(np.array([0, 1]))
	training_inputs.append(np.array([0, 0]))

	labels = np.array([1, 1, 1, 0])

	P = Perceptron(func=f1,n_inputs=2)
	P.train(training_inputs,labels)

	V = ['rs','b^']
	for teste in training_inputs:
		pred = P.predict(teste)
		plt.plot(teste[0],teste[1],V[pred])
	plt.xlabel('X')
	plt.ylabel('Y')
	plt.title('OR Logic Perceptron')
	red = mpatches.Patch(color='red', label='False = 0')
	blue = mpatches.Patch(color='blue', label='True = 1')
	plt.legend(handles=[red,blue])
	plt.show()

	#Xor Logic Perceptron
	training_inputs = []
	training_inputs.append(np.array([1, 1]))
	training_inputs.append(np.array([1, 0]))
	training_inputs.append(np.array([0, 1]))
	training_inputs.append(np.array([0, 0]))

	labels = np.array([0, 1, 1, 0])

	P = Perceptron(func=f1,n_inputs=2)
	P.train(training_inputs,labels)

	V = ['rs','b^']
	for teste in training_inputs:
		pred = P.predict(teste)
		plt.plot(teste[0],teste[1],V[pred])
	plt.xlabel('X')
	plt.ylabel('Y')
	plt.title('XOR Logic Perceptron')
	red = mpatches.Patch(color='red', label='False = 0')
	blue = mpatches.Patch(color='blue', label='True = 1')
	plt.legend(handles=[red,blue])
	plt.show()
	
	#Iris Perceptron training = teste 
	df = pd.read_csv(r"iris.csv")
	species = df[['Species']]
	species = species.values
	species = Mat2Vec(species)
	
	df = df[['Sepal length','Sepal width','Petal length','Petal width']]

	df = df.values
	training_inputs = []
	for i in df:
		training_inputs.append(i)

	P = Perceptron(func=f1,n_inputs=4)
	P.train(training_inputs,species)
	V = ['rs','b^']
	for teste in training_inputs:
		pred = P.predict(teste)
		plt.plot(float(teste[2]),float(teste[3]),V[pred])
	plt.xlabel('Petal length')
	plt.ylabel('Petal width')
	plt.title('Iris Setosa vs Versicolor Perceptron')
	red = mpatches.Patch(color='red', label='setosa')
	blue = mpatches.Patch(color='blue', label='versicolor')
	plt.legend(handles=[red,blue])
	plt.show()
