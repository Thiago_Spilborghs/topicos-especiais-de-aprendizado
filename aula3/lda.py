import numpy as np
import numpy.linalg as lg
import util as ut
import matplotlib.pyplot as plt
from sklearn import datasets
import matplotlib.patches as mpatches

def vec2matx(V):
	Mat = []
	Mat.append(V)
	return Mat

def vec_sub(X,Y):
	mean = []
	for i in X:
		mean.append(i)
	for i in range(0,len(Y)):
		mean[i] = mean[i] - Y[i]
	return mean

def matrix_plus(X,Y):
	result = []
	for i in range(0,len(X)):
		result.append([])
		for j in range(0,len(X[0])):
			result[i].append(X[i][j]+Y[i][j])
	return result

def matrix_N(X,n):
	result = []
	for i in range(0,len(X)):
		result.append([])
		for j in range(0,len(X[0])):
			result[i].append(X[i][j]*n)
	return result

def sample_mean(X,Y,c):
	mean = []
	N = 0.0
	for i in range(0,len(X[0])):
		mean.append(0.0)

	for i in range(0,len(Y)):
		if i == c:
			N += 1
			for j in range(0,len(mean)):
				mean[j] = mean[j] + X[i][j]

	for i in range(0,len(mean)):
		mean[i] = mean[i] / N
	
	return mean


def sample_cov(X,Y,c):
	cov = 0.0
	mean = sample_mean(X,Y,c)
	N = 0.0
	for i in range(0,len(Y)):
		if Y[i] == c:
			N += 1
			cov = cov + ut.matmult(vec2matx(vec_sub(X[i],mean)),ut.transposta(vec2matx(vec_sub(X[i],mean))))[0][0]

	cov = cov / (N-1)

	return cov

def eigen_vv(vec,value):
	result = []
	for i in vec:
		result.append(i*value)
	return result

def eigen_mv(vec,value):
	result = []
	for i in vec:
		result.append(i+value)
	return result

def N_values(Y,c):
	x = 0
	for i in Y:
		if i == c:
			x += 1
	return x

def Grand_mean(X,Y):
	mean = []
	N = len(Y)
	for i in X[0]:
		mean.append(0.0)
	for i in X:
		for j in range(0,len(i)):
			mean[j] += i[j]

	for i in range(0,len(mean)):
		mean[i] = mean[i] / N

	return mean

def normalize(X,Y):
	media = Grand_mean(X,Y)
	norm = []
	for i in X:
		norm.append(vec_sub(i,media))

	return norm

def Reduce_dt(dt,n=2):
	Vec = []
	for i in range(0,len(dt)):
		Vec.append([])
		for j in range(0,n):
			Vec[i].append(dt[i][j])

	return Vec

def mean(V):
	media = 0.
	for i in V:
		media +=i
	media = media/len(V)
	return media

def Data_Adjust(X):
	x_ = mean(X)
	A = []
	for i in X:
		A.append(i-x_)
	return A

def Cov(X,Y):
	x_ = mean(X)
	y_ = mean(Y)
	Cov = 0
	for i in range(0,len(X)):
		Cov += (X[i]-x_) * (Y[i]-y_)
	Cov = Cov/(len(X)-1)
	return Cov

def Cov_Mat(V):
	CovM = []
	for i in range(0,len(V)):
		CovM.append([])
		for j in range(0,len(V)):
			c = Cov(V[i],V[j])
			CovM[i].append(c)
	return CovM 

def exclude_X(M):
	V = []
	for i in M:
		V.append(i[0])

	return V

def exclude_Y(M):
	V = []
	for i in M:
		V.append(i[1])

	return V

def exclude_Z(M):
	V = []
	for i in M:
		V.append(i[2])

	return V

def exclude_W(M):
	V = []
	for i in M:
		V.append(i[3])

	return V

def ord_vec(V):
	Vord = []
	for i in V:
		Vord.append(i)
	print 
	Vord.sort()
	return Vord

def PCA(M,size):
	V = []
	VX = Data_Adjust(exclude_X(M))
	V.append(VX)
	VY = Data_Adjust(exclude_Y(M))
	V.append(VY)
	if size >= 3:
		VZ = Data_Adjust(exclude_Z(M))
		V.append(VZ)
	if size == 4:
		VW = Data_Adjust(exclude_W(M))
		V.append(VW)
	Matrix = Cov_Mat(V)
	Eigval, Eigvec = lg.eig(Matrix)
	egvec = Eigvec.real
	egval = Eigval.real
	Vreal = []
	for i in egval:
		Vreal.append(float(i))

	vec_ord = ord_vec(Vreal)
	VEig = []
	if size == 2:
		size = size + 1
	for i in range(0,size-1):
		index = ut.index(vec_ord[i],Eigval)
		VEig.append(Eigvec[index])

	resp = ut.matmult(M,ut.transposta(VEig))

	return resp
	

def LDA(lda,pca=False):
	LDA = lda
	iris = datasets.load_iris()

	dt = iris['data']
	result = iris['target']

	dt = Reduce_dt(dt,LDA)
	if pca == True:
		dt = PCA(dt,LDA)

	#Calculate Between
	Sb = []
	for i in range(0,len(dt[0])):
		Sb.append([])
		for j in range(0,len(dt[0])):
			Sb[i].append(0.0)

	Gmean = Grand_mean(dt,result)
	for i in range(0,3):
		N = N_values(result,i)
		sm = sample_mean(dt,result,i)

		parcial = matrix_N(ut.matmult(ut.transposta(vec2matx(vec_sub(sm,Gmean))),vec2matx(vec_sub(sm,Gmean))),N)
		Sb = matrix_plus(Sb,parcial)


	#Calculate Within
	Sw = []
	for i in range(0,len(dt[0])):
		Sw.append([])
		for j in range(0,len(dt[0])):
			Sw[i].append(0.0)

	for i in range(0,len(dt)):
		c = result[i]
		x = dt[i]
		sm = sample_mean(dt,result,c)
		parcial = ut.matmult(ut.transposta(vec2matx(vec_sub(x,sm))),vec2matx(vec_sub(x,sm)))
		Sw = matrix_plus(Sw,parcial)


	A = ut.matmult(ut.Inversa(Sw),Sb)

	Eigval, Eigvec = lg.eig(A)

	eg1, eg2 = ut.two_bigg(Eigval)

	eg1 = ut.index(eg1,Eigval)

	eg2 = ut.index(eg2,Eigval)

	mataux = []
	mataux.append(Eigvec[eg1])
	mataux.append(Eigvec[eg2])


	color = ['ro','go','bo']
	a = 0
	for i in dt:
		plt.plot(i[eg1],i[eg2],color[result[a]])
		a += 1

	if pca == True:
		plt.title('Iris with '+str(LDA)+' Components + PCA')
	else:
		plt.title('Iris with '+str(LDA)+' Components')
	plt.xlabel('LDA2')
	plt.ylabel('LDA1')
	red_patch = mpatches.Patch(color='red', label='setosa')
	green_patch = mpatches.Patch(color='green', label='versicolor')
	blue_patch = mpatches.Patch(color='blue', label='virginica')
	plt.legend(handles=[red_patch,green_patch,blue_patch])
	plt.show()

if __name__ == '__main__':
	LDA(4,pca=True)
	LDA(3,pca=True)
	LDA(2,pca=True)
	LDA(4)
	LDA(3)
	LDA(2)